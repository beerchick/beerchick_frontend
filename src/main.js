import Vue from "vue";
import App from "./App.vue";
import router from "./router";

import * as VueGoogleMaps from "vue2-google-maps";

Vue.config.productionTip = false;

Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyDVLjz8ddghc0TXXv9F6xqr_lje8tBgLFU",
    libraries: "places,visualization"
  }
});

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
