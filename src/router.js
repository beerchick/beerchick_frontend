import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/bar/:id",
      name: "bar",
      component: () => import("./views/Bar.vue")
    },
    {
      path: "/beer/:id",
      name: "beer",
      component: () => import("./views/Beer.vue")
    },
    {
      path: "/user/:id",
      name: "user",
      component: () => import("./views/User.vue")
    }
  ]
});
