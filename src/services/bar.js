import axios from "axios";

const urlPrefix = process.env.VUE_APP_API_ENDPOINT_URL;

export const fetchBar = id => {
  return axios.get(`${urlPrefix}/bar/${id}/`);
};

export const fetchNearestBars = (lat, lon, distance) => {
  return axios.get(
    `${urlPrefix}/bar/nearest/?lat=${lat}&lon=${lon}&distance=${
      distance > 2 ? distance : 2
    }`
  );
};

export const fetchSuggestions = prefix => {
  return axios.get(`${urlPrefix}/bar/list?search=${prefix}&term=match`);
};
