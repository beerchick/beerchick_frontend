import axios from "axios";

const urlPrefix = process.env.VUE_APP_API_ENDPOINT_URL;

export const fetchBeer = id => {
  return axios.get(`${urlPrefix}/beer/${id}/`);
};
