import axios from "axios";

const urlPrefix = process.env.VUE_APP_API_ENDPOINT_URL;

export const fetchUser = id => {
    return axios.get(`${urlPrefix}/user/${id}/`);
};